/**
 * index.js - Loads the simple On/Off adapter.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.*
 */

'use strict';

const Co2MonitorAdapter = require('./co2-monitor-adapter');

module.exports = (addonManager, manifest) => {
  new Co2MonitorAdapter(addonManager, manifest);
};
